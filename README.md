# Application Setup

### Project structure

The project is backed by MySQL database.

It contains two git submodules : 
  - lpg-product - the product Restful API written in Spring boot
  - lpg-shop-ui - the product UI writte in ReactJS

You have to update the submodules by running:

  ```bash
  git submodule init
  git submodule update
  ```

### Run the application

#### Prerequisite

You use docker compose to run the application

- Install Docker

After installing docker, make ensure the following network ports is not occupied: 
  3306, 8080, 8000

- Run the application

Run the docker compose command to bring all services:

```bash
docker-compose up
```

- Load the testing data

This is recommended to not load the testing data in Production environment automatically, you may load the data into MySQL server manually.

```
src/main/resources/db/testdata/afterMigrate.sql
```

- Open [http://localhost:8000](http://localhost:8000) with your browser to see the result

### Uncompleted element due to time restraints

- Authentication and Authorization
- Add/Update/Delete Products in the frontend
- Group products by category
- Purchase product related features
- Cursor style products pagination